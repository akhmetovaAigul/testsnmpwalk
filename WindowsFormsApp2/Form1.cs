﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lextm.SharpSnmpLib;
using Lextm.SharpSnmpLib.Messaging;
using Lextm.SharpSnmpLib.Security;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public static List<ConfigFileClass> ConfList = new List<ConfigFileClass>();
        private const string StringConToObject = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source = C:/Base/object/Object.mdb ";
        public class ConfigFileClass
        {
            public ConfigFileClass(string ipAddress, string oid, int countPort, int numberEventInDb, IList<Variable> resultSnmp, object[] newRow)
            {
                IpAddress = ipAddress;
                CountPort = countPort;
                NumberEventInDb = numberEventInDb;
                Oid = oid;
                ResultSnmp = resultSnmp;
                NewRow = newRow;
            }

            public string IpAddress { get; }
            public string Oid { get; }
            public int CountPort { get; }
            public int NumberEventInDb { get; }
            public Dictionary<string, int> DicOid = new Dictionary<string, int>();
            public IList<Variable> ResultSnmp;
            public object[] NewRow;

            public static void CreatConfig()
            {
                try
                {
                    var selectStringFromConfigScadaAlarms = "SELECT ipAddress,OidTree,CountPort,NumberEventInDB FROM ScadaAlarmsSnmp ";
                    OleDbDataReader readerFromConfigScadaAlarms;
                    OleDbConnection tmpCn;
                    OleDbDataReader readerPorts = null;
                    tmpCn = new OleDbConnection(StringConToObject);
                    var configScadaAlarmsCmd = new OleDbCommand(selectStringFromConfigScadaAlarms, tmpCn);
                    tmpCn.Open();
                    readerFromConfigScadaAlarms = configScadaAlarmsCmd.ExecuteReader();
                    while (readerFromConfigScadaAlarms != null && readerFromConfigScadaAlarms.Read())
                        ConfList.Add(new ConfigFileClass(
                            readerFromConfigScadaAlarms["ipAddress"].ToString(),
                            readerFromConfigScadaAlarms["OidTree"].ToString(),
                            Convert.ToInt32(readerFromConfigScadaAlarms["CountPort"].ToString()),
                             Convert.ToInt32(readerFromConfigScadaAlarms["NumberEventInDB"].ToString()),
                             new List<Variable>(),
                             new object[3]
                             ));

                    readerFromConfigScadaAlarms?.Close();
                    string ports = "";
                    foreach (var item in ConfList)
                    {
                        string[] many = new string[item.CountPort + 1];
                        for (int i = 1; i <= item.CountPort; i++)
                        {
                            if (i == item.CountPort)
                            {
                                many[i] = "oidPort" + i + " ";
                            }
                            else
                            {
                                many[i] = "oidPort" + i + ", ";
                            }


                        }
                        ports = string.Concat(many);
                        var selectStringPortOid = "SELECT " + ports + " From ScadaAlarmsSnmp";
                        configScadaAlarmsCmd = new OleDbCommand(selectStringPortOid, tmpCn);
                        readerPorts = configScadaAlarmsCmd.ExecuteReader();
                        while (readerPorts != null && readerPorts.Read())
                            for (int i = 1; i <= item.CountPort; i++)
                            {
                                var test = readerPorts["OidPort" + i].ToString();
                                item.DicOid[test] = 1;
                            }

                    }

                    readerFromConfigScadaAlarms?.Close();
                    readerPorts?.Close();
                    
                }
                catch (Exception e)
                {

                }

            }
        }

        public int[] StringToInt(string ipaddress)
        {
            var test = ipaddress.Split('.');
            int[] myInts = Array.ConvertAll(test, s => int.Parse(s));
            return myInts;
        }

        public IList<Variable> SnmpWalk(string ipAddress, string oid, int countPort, int numberEventInDb, Dictionary<string, int> icOid, IList<Variable> resultVariables)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            int timeout = 1000;
            int maxRepetitions = 20;
            Levels level = Levels.Privacy;
            string user = "Admin";
            string contextName = string.Empty;
            string authentication = "MD5";
            string authPhrase = "Transneft2$";
            string privPhrase = "Transneft2$";
            WalkMode mode = WalkMode.WithinSubtree;
            IAuthenticationProvider auth = GetAuthenticationProviderByName(authentication, authPhrase);
            byte[] addresCisco = StringToInt(ipAddress).Select(x => Convert.ToByte(x)).ToArray();
            var ipCisco = new IPAddress(addresCisco);
            ObjectIdentifier test = new ObjectIdentifier(oid);
            //IList<Variable> resultCisco = new List<Variable>();
            IPEndPoint receiverCisco = new IPEndPoint(ipCisco, 161);
            Discovery discovery = Messenger.GetNextDiscovery(SnmpType.GetNextRequestPdu);
            IPrivacyProvider priv;
            if ((level & Levels.Privacy) == Levels.Privacy)
            {
                if (DESPrivacyProvider.IsSupported)
                {
                    priv = new DESPrivacyProvider(new OctetString(privPhrase), auth);
                }
                else
                {
                    Console.WriteLine(@"DES (ECB) is not supported by .NET Core.");
                    return resultVariables;
                }
            }
            else
            {
                priv = new DefaultPrivacyProvider(auth);
            }
            try
            {
                ReportMessage reportCisco1 = discovery.GetResponse(timeout, receiverCisco);
                Messenger.BulkWalk(VersionCode.V3, receiverCisco, new OctetString(user), new OctetString(string.IsNullOrWhiteSpace(contextName) ? string.Empty : contextName), test, resultVariables, timeout, maxRepetitions, mode, priv, reportCisco1);
                watch.Stop();
                Debug.WriteLine(watch.Elapsed + " время выполнения snmp");
                return resultVariables;

            }
            catch (Exception e)
            {
               
                return resultVariables;
                //throw;
            }
        }

        private static IAuthenticationProvider GetAuthenticationProviderByName(string authentication, string phrase)
        {
            if (authentication.ToUpperInvariant() == "MD5")
            {
                return new MD5AuthenticationProvider(new OctetString(phrase));
            }

            if (authentication.ToUpperInvariant() == "SHA")
            {
                return new SHA1AuthenticationProvider(new OctetString(phrase));
            }

            throw new ArgumentException(@"unknown name", nameof(authentication));
        }
        public Form1()
        {
            InitializeComponent();
        }

        public static bool PingHost(string ipAddress)
        {
            try
            {
                Ping myPing = new Ping();
                PingReply reply = myPing.Send(ipAddress, 1000);
                if (reply != null && reply.Status == IPStatus.Success)
                {
                    //Logger.Debug(ipAddress + " is Online");
                    return true;
                }
                else
                {
                    //Logger.Debug(ipAddress + " is Offline");
                    return false;
                }

            }
            catch (Exception e)
            {
                //Logger.Error(e + " " + ipAddress + " is Error");
                return false;
            }

        }

        private void timerSNMP_Tick(object sender, EventArgs e)
        {
            Stopwatch watch = new Stopwatch();
            Stopwatch watch1 = new Stopwatch();
            watch.Start();
            int i = 0;
            while (i <= ConfList.Count)
            {
                foreach (var item in ConfList)
                {
                    if (PingHost(item.IpAddress))
                    {
                        item.ResultSnmp = SnmpWalk(item.IpAddress, item.Oid, item.CountPort, item.NumberEventInDb, item.DicOid, item.ResultSnmp);
                        watch1.Start();
                        foreach (var value in item.ResultSnmp)
                            //если id из запроса присутствует в словаре И его значение отличается от того что находится в словаре
                            if (item.DicOid.ContainsKey(value.Id.ToString()) && item.DicOid[value.Id.ToString()].ToString() != value.Data.ToString())
                            {
                                var port1 = Convert.ToInt32(value.Id.ToString().Substring(value.Id.ToString().Length - 1));
                                if (value.Data.ToString() == "1")
                                {
                                    item.NewRow[0] = DateTime.Now;
                                    item.NewRow[1] = "Квитировать";
                                    item.NewRow[2] = item.NumberEventInDb - item.CountPort + 8 * port1;
                                }
                                else
                                {
                                    item.NewRow[0] = DateTime.Now;
                                    item.NewRow[1] = "Квитировать";
                                    item.NewRow[2] = item.NumberEventInDb - item.CountPort + 8 * port1 + 1;
                                }
                                item.DicOid[value.Id.ToString()] = Convert.ToInt32(value.Data.ToString());
                            }
                        watch1.Stop();
                        Debug.WriteLine(watch.Elapsed + " время выполнения сортировки результатов" + item.IpAddress);
                    }
                }
                i = i + 2;
            }

            foreach (var item in ConfList)
                //textBox1.Text = item.NewRow; вывод на экран
            watch.Stop();
            Debug.WriteLine(watch.Elapsed + " время выполнения таймера");
        }
    }
}
